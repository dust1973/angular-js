export interface Post {
  id: bigint;
  title: string;
  content: string;
}

