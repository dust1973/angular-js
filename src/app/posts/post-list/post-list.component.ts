import {Component, OnInit} from '@angular/core';
import {Post} from '../post.model';
import {PostsService} from '../posts.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts: Post[] = [];
  private postsSubscription : Subscription;

  constructor(public postService: PostsService) {
  }

  ngOnInit(): void {
    this.postService.getPosts();
    this.postsSubscription = this.postService.getPostUpdateListener().subscribe(
      (posts : Post[]) => {
        this.posts = posts;
      }
    );
  }

  ngOnDestroy(): void{
    this.postsSubscription.unsubscribe();
  }

}
