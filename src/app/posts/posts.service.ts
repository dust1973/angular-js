import {Injectable, OnInit} from '@angular/core';
import {Post} from "./post.model";
import {Subject} from 'rxjs';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private _posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();


  constructor(private http : HttpClient) {
  }

  getPosts(){
    // HTTP request
    this.http.get<{mesage:string, posts: Post[]}>('http://localhost:3000/api/posts').subscribe(
      (postData)=> {
          this._posts = postData.posts;
          this.postsUpdated.next([...this._posts]);
      }
    );
  }

  getPostUpdateListener(){
    return this.postsUpdated.asObservable();
  }

  addPost(id:bigint, title:string, content:string){
    const post : Post = {id: id, title: title, content : content}
    this._posts.push(post);
    this.postsUpdated.next([...this._posts]);
  }

}
