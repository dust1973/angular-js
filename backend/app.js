const express = require('express');
const app = express();
/*app.use((req, res, next) => {
  console.log('First middleware');
  next();
});*/

app.use('/api/posts', (req, res, next) => {

  const posts = [
    {id:'141241234', title:'1st post', content: 'Our first posts content'},
    {id:'545241234', title:'2st post', content: 'Our second posts content'},
  ];

  res.status(200).json({
      message: 'Post fetched successfully!',
      posts: posts
  });
});

module.exports = app;
